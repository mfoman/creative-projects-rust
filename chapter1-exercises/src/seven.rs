/**
 * Generate 10 i32 numbers between 100 and 400
 */
use rand::Rng;

pub fn run(level: u8) {
    let mut rng = rand::thread_rng();

    let numbers: Vec<i32> = (0..level).map(|_| {
        rng.gen_range(100, 401)
    }).collect();

    println!("{:?}", numbers);
}