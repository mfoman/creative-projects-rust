mod six;
mod seven;
#[macro_use]
extern crate log;
use structopt::StructOpt;

#[derive(StructOpt, Debug)]
struct Opt {
    #[structopt(short = "l", long = "level")]
    value: u8
}

fn main() {
    env_logger::init();

    // info!("Init logger");
    // error!("Error");
    // warn!("Warn");
    // debug!("Debug");

    println!("{:?}", Opt::from_args());

    let Opt { value: level } = Opt::from_args();

    if !(0..=20).contains(&level) {
        warn!("It has to be between 0 and 20");
        std::process::exit(0);
    }

    // Mods
    six::run(level);
    seven::run(level);
}
