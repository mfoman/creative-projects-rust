/**
 * Write a program that generates 10, f32 pseudo-random numbers between 100 and 400.
 */
use rand::Rng;

pub fn run(level: u8) {
    let mut rng = rand::thread_rng();

    let numbers: Vec<f32> = (0..level).map(|_| {
        rng.gen_range(100.0, 401.0)
    }).collect();
    
    println!("{:?}", numbers);
}