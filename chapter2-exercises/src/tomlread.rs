pub fn run(path: &String) {
    let config = {
        let text = std::fs::read_to_string(path).unwrap();
        text.parse::<toml::Value>().unwrap()
    };

    println!("{:#?}", config);
    println!("Database {}",
        config.get("postgresql").unwrap()
        .get("database").unwrap()
        .as_str().unwrap()
    );
}