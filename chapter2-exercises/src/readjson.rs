use serde_json::{Number, Value};
use serde_derive::{Deserialize, Serialize};

#[derive(Deserialize, Serialize, Debug)]
struct Product {
    id: u32,
    category: String,
    name: String,
}

#[derive(Deserialize, Serialize, Debug)]
struct Sale {
    id: String,
    product_id: u32,
    date: i64,
    quantity: f64,
    unit: String
}

#[derive(Deserialize, Serialize, Debug)]
struct SaleAndProducts {
    products: Vec<Product>,
    sales: Vec<Sale>
}

pub fn static_read_run(infile: &str, outfile: &str) {
    let mut salesandproducts = {
        let content = std::fs::read_to_string(&infile).unwrap();

        serde_json::from_str::<SaleAndProducts>(&content).unwrap()
    };

    salesandproducts.sales[1].quantity += 1.5;

    std::fs::write(outfile, serde_json::to_string_pretty(&salesandproducts).unwrap()).unwrap();
}

pub fn run(infile: &str, outfile: &str) {
    let mut sales = {
        let txt = std::fs::read_to_string(&infile).unwrap();

        serde_json::from_str::<Value>(&txt).unwrap()
    };

    if let Value::Number(n) = &sales["sales"][1]["quantity"] {
        sales["sales"][1]["quantity"] =
            Value::Number(
                Number::from_f64(
                    n.as_f64().unwrap() + 1.5
                ).unwrap()
            );
    }


    std::fs::write(
        outfile,
        serde_json::to_string_pretty(&sales).unwrap()
    ).unwrap();
}