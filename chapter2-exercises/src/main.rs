#![allow(dead_code)]
mod tomlread;
mod readjson;

fn main() {
    // let path = std::env::args().nth(1).unwrap();
    let args: Vec<String> = std::env::args().collect();

    // tomlread::run(&args[1]);
    readjson::static_read_run(&args[1], &args[2]);
}
